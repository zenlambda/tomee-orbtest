# Instructions

Run with:

 mvn clean install tomee:run

This should produce the following error:

    SEVERE: EjbTransactionUtil.handleSystemException: Error creating corba service
    java.lang.RuntimeException: Error creating corba service
        at com.example.tomee_orbtest.CORBAServiceBean.postConstruct(CORBAServiceBean.java:36)

    ...

    Caused by: org.omg.CORBA.NO_IMPLEMENT:   vmcid: SUN  minor code: 201  completed: No
        at com.sun.corba.se.impl.logging.ORBUtilSystemException.genericNoImpl(ORBUtilSystemException.java:8377)
        at com.sun.corba.se.impl.logging.ORBUtilSystemException.genericNoImpl(ORBUtilSystemException.java:8399)
        at com.sun.corba.se.impl.orb.ORBSingleton.resolve_initial_references(ORBSingleton.java:334)
        at com.example.tomee_orbtest.CORBAServiceBean.init(CORBAServiceBean.java:23)
        at com.example.tomee_orbtest.CORBAServiceBean.postConstruct(CORBAServiceBean.java:34)
        ... 46 more
     
