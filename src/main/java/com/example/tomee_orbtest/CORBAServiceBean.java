package com.example.tomee_orbtest;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import org.omg.CORBA.ORB;
import org.omg.CORBA.ORBPackage.InvalidName;
import org.omg.PortableServer.POA;
import org.omg.PortableServer.POAHelper;
import org.omg.PortableServer.POAManagerPackage.AdapterInactive;


@Singleton
@Startup
public class CORBAServiceBean 
{
	@Resource
	private ORB containerOrb;
    
    private void init(ORB orb) throws InvalidName, AdapterInactive {
    	org.omg.CORBA.Object root = orb.resolve_initial_references("RootPOA");
        POA rootPoa = POAHelper.narrow(root);
        rootPoa.the_POAManager().activate();
        
        // cannot continue
    }
    
    @PostConstruct
    public void postConstruct() {
    	try {
			init(containerOrb);
		} catch (Exception e) {
			throw new RuntimeException("Error creating corba service",e);
		}
    }
}
